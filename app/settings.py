JENKINS_USER_ID = 'username'
JENKINS_API_TOKEN = 'secret_token'
MAIL_SERVER = ''
MAIL_DEFAULT_SENDER = 'jenkins-proxy@example.org'
ADMIN_EMAILS = ['admin@example.org']
ALLOWED_JENKINS_DOMAINS = ('example.org',)
