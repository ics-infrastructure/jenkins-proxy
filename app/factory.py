# -*- coding: utf-8 -*-
"""
app.factory
~~~~~~~~~~~

Create the WSGI application.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
from flask import Flask
from .main import bp as main
from . import settings


def create_app(config=None):
    app = Flask(__name__)

    app.config.from_object(settings)
    app.config.from_envvar('LOCAL_SETTINGS', silent=True)
    app.config.update(config or {})

    if not app.debug:
        import logging
        if app.config['MAIL_SERVER']:
            # Send ERROR via mail
            from logging.handlers import SMTPHandler
            mail_handler = SMTPHandler(app.config['MAIL_SERVER'],
                                       fromaddr=app.config['MAIL_DEFAULT_SENDER'],
                                       toaddrs=app.config['ADMIN_EMAILS'],
                                       subject='jenkins-proxy: ERROR raised')
            mail_handler.setFormatter(logging.Formatter("""
                Message type:       %(levelname)s
                Location:           %(pathname)s:%(lineno)d
                Module:             %(module)s
                Function:           %(funcName)s
                Time:               %(asctime)s

                Message:

                %(message)s
            """))
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)
        # Log to stderr by default
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'
        ))
        # Set app logger level to DEBUG
        # otherwise only WARNING and above are propagated
        app.logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
        app.logger.addHandler(handler)
    app.logger.info('jenkins-proxy created!')

    app.register_blueprint(main)

    return app
