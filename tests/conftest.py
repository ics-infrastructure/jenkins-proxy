# -*- coding: utf-8 -*-
"""
tests.conftest
~~~~~~~~~~~~~~

Pytest fixtures common to all tests.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import json
import pytest
import requests_mock as rm_module
from app.factory import create_app


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    config = {
        'TESTING': True,
        'ALLOWED_JENKINS_DOMAINS': ('mycompany.com',),
    }
    app = create_app(config=config)
    app.logger.disabled = True
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture
def client(request, app):
    return app.test_client()


@pytest.fixture
def requests_mock(request):
    with rm_module.Mocker() as mocker:
        yield mocker


@pytest.fixture
def bitbucket_commit_data():
    return {
        'push': {
            'changes': [
                {
                    'new': {
                        'type': 'branch'
                    }
                }
            ]
        }
    }


@pytest.fixture
def gitlab_commit_data():
    return {
        'object_kind': 'push',
        'after': '3fdf1464a41a3c42405bab7408829714595cde01',
        'ref': 'refs/heads/master',
        'user_name': 'Benjamin Bertrand',
        'project': {
            'http_url': 'https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-and-role-test.git'
        },
        'commits': [
            {'id': '3fdf1464a41a3c42405bab7408829714595cde01',
             }
        ]
    }
