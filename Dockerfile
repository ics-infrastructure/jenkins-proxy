FROM continuumio/miniconda3:latest

MAINTAINER Benjamin Bertrand "benjamin.bertrand@esss.se"

RUN groupadd -r ics && useradd -r -g ics ics

WORKDIR /app

COPY environment.yml /app/environment.yml
RUN conda config --add channels conda-forge \
    && conda env create -n jenkins-proxy -f environment.yml \
    && rm -rf /opt/conda/pkgs/*

COPY . /app/
RUN chown -R ics:ics /app

USER ics

ENV LOCAL_SETTINGS /app/settings.cfg
ENV FLASK_APP /app/wsgi.py
# activate the jenkins-proxy environment
ENV PATH /opt/conda/envs/jenkins-proxy/bin:$PATH

EXPOSE 5000
CMD ["uwsgi", "/app/uwsgi.ini"]
