# Jenkins Proxy

A proxy that transforms Bitbucket and Gitlab webhooks call to a format that can trigger Jenkins parameterized builds.
By default it only triggers a build when pushing tags.
The optional `forward` parameter can be set to 'last_commit' to forward the last commit.

This started as a fork from https://github.com/akhy/jenkins-bitbucket-webhook-proxy but was almost re-written from scratch.

## Why?

1. Bitbucket's webhook can only post **JSON** requests
2. **Change details** (e.g. commit author, commit SHA, etc) of Bitbucket's webhook calls is located **in the request body**
3. Jenkins' remotely triggered jobs (via webhook) **cannot inspect the request body**
4. We need commit details from webhook to be **available as build parameters** in Jenkins jobs
   so we can checkout and build **only those specific commits**

## Setup

### 1. Run the proxy

#### Developement

1. Clone the repository

    ```
    $ git clone https://github.com/akhyrul/jenkins-bitbucket-webhook-proxy jenkins-proxy
    $ cd jenkins-proxy
    ```

2. Create a settings.cfg file with jenkins user id and API token:

    ```
    JENKINS_USER_ID = 'username'
    JENKINS_API_TOKEN = 'xxxxxxxxx'
    ```

There are two methods to run the proxy.

##### Running the script with Python

The recommended method is to use [conda](https://conda.io/docs/index.html):

```
$ conda env create -f environment.yml
$ source activate jenkins-proxy
(jenkins-proxy) $ python wsgi.py
```

There is a requirements.txt if you prefer to use virtualenv.

##### Running with Docker

```
$ docker build -t jenkins-proxy .
$ docker run -p 5000:5000 -v $(pwd):/app -i -t jenkins-proxy
```

or:

```
$ make build
$ make test
$ make run
```

#### Production

The recommended way is to use docker:

1. Create a settings.cfg file with jenkins user id and API token:

    ```
    JENKINS_USER_ID = 'username'
    JENKINS_API_TOKEN = 'xxxxxxxxx'
    ```

    To send errors via mail, define as well the following variables:

    ```
    MAIL_SERVER = 'smtp.example.org'
    MAIL_DEFAULT_SENDER = 'jenkins-proxy@example.org'
    ADMIN_EMAILS = ['admin@example.org']
    ```

2. Use gunicorn to run the application:

    ```
    $ docker run -d --restart always --name jenkins-proxy -v $(pwd)/settings.cfg:/app/settings.cfg:ro -p 8000:8000 -it jenkins-proxy_image gunicorn -w 3 -b 0.0.0.0:8000 wsgi:app
    ```

3. You probably want to run the application behind nginx. See [flask documentation](http://flask.pocoo.org/docs/0.12/deploying/wsgi-standalone/#proxy-setups).

### 2. Setting up Bitbucket and Jenkins

#### Jenkins side

1. Open up your job configuration
2. Check **"This build is parameterized"** and add the string parameters:
   -  git_tag
   -  git_hash
   -  repo_url
   -  username
3. In build triggers section check **Trigger builds remotely** and enter any random authentication token. This token will be used in the next Bitbucket configuration.
4. Define your pipeline script. You can access as variables the string parameters passed.

#### Bitbucket side

Create a webhook with **repository push** trigger and this URL template:

> https://**myproxy.mycompany.com**/build?jenkins=**https://jenkins.mycompany.com:8080**&job=**job**&token=**token**

- **myproxy.mycompany.com** is where your proxy is listening
- **jenkins** query param is your Jenkins URL
- **job** query param is the Jenkins job's name (see the URL when you're editing the job)
- **token** query param is the token you have set in the Jenkins job before

You can add the optional **forward** query parameter to forward the last commit (and not only tags).
Set it to 'last_commit' in that case.

Note: Currently the proxy only handles repository push trigger.

The webhook can be created using the Bitbucket API by running the **bitbucket.pex** script.
